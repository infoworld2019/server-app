import Koa from 'koa';
import {errorHandler, logger} from './utils';
import itemRouter from './item/itemRouter';
import bodyParser from 'koa-bodyparser';
import WebSocket from 'ws';
import {init} from './core';
import jwt from 'koa-jwt';
import cors from '@koa/cors';
import {jwtConfig, router as authRouter} from './auth';
import Router from 'koa-router';

const app = new Koa();

const server = require('http').createServer(app.callback());
const wss = new WebSocket.Server({server});
init(wss);
wss.on('connection', ws => {
    console.log('Client connected!');
    ws.on('message', message => {
        console.log('received: %s', message);
    });
});


app.use(bodyParser());
app.use(cors());
let count = 0;
app.use(logger);
app.use(errorHandler);

// app.use(async (ctx, next) => {
//     await new Promise(resolve => {
//         setTimeout(resolve, 1000);
//     });
//     await next();
// });

const prefix = '/api';
// public
const publicApiRouter = new Router({ prefix });
publicApiRouter
  .use('/auth', authRouter.routes());
app
  .use(publicApiRouter.routes())
  .use(publicApiRouter.allowedMethods());
// for decoding
app.use(jwt(jwtConfig));

const protectedApiRouter = new Router({ prefix });
protectedApiRouter
    .use('/item', itemRouter.routes());
app
    .use(protectedApiRouter.routes())
    .use(protectedApiRouter.allowedMethods());


server.listen(3000);

// read about Promise
