import WebSocket from "ws";

let wss;
export const init = webSocketServer => {
    wss = webSocketServer;
}
export const broadcast = data => {
    if (!wss)
        return;
    wss.clients.forEach((client)=>{
        if(client.readyState === WebSocket.OPEN){
            client.send(JSON.stringify(data));
        }
    });
}

