import Router from 'koa-router';
import ItemStore from './ItemStore'
import {broadcast} from '../core';

export const router = new Router();

const items = new ItemStore();

router.get('/', async (ctx, next) => {
    ctx.response.body = await items.find({});
    ctx.response.status = 200;
});

router.get('/:id', async (ctx, next) => {
    const id = ctx.params.id;
    const item = await items.findOne(id);
    if(item){
        ctx.response.body = item;
        ctx.response.status = 200;
    }
    else{
        ctx.response.status = 404;
    }
});


router.post('/', async (ctx, next) => {
    const item = ctx.request.body;
    let insertedItem = await items.insert(item);
    ctx.response.body = insertedItem;
    ctx.response.status = 200;
    broadcast(insertedItem);
});

router.put('/:id', async (ctx, next) => {
    const item = ctx.request.body;
    // ctx.params.id;
    await items.update({ _id: item._id }, item);
    ctx.body = 'The item was updated!';
    ctx.response.status = 200;

});

router.delete('/', async (ctx, next) => {
    const item = ctx.request.body;
    await items.remove(item);
    ctx.response.body = "Item was successfully deleted";
    ctx.response.status = 200;
});

export default router;
