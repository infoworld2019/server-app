import { Issue, SEVERITY } from '../core'; //acolade, pt import, apoi numele exportate

function Item(text, isActive) {
    this.text = text;
    this.isActive = isActive;
    this.updated = Date.now();
    this.version = 1;
}
//orice functie are proprietatea prototype - obiectul care contine functiile comune pe care le au toate obiectele de tip item
Item.prototype.toString = function() {
    return `${this.text},${this.isActive}` //backtick, pt evaluarea expresiilor prin concatenare,
};

Item.prototype.validate = function() {
    const issues = [];
       //! this.text -> not undefined, not null, not ''
    if (!this.text || typeof this.text !== 'string' || this.text.trim().length === 0) {
        issues.push(new Issue(SEVERITY.WARNING, 'text', 'Invalid text property'));
    }
    return issues;
};

export default Item; //export pt utilizare -> in general un modul exporta un singur obiect, cel care importa poate sa dea orice nume obiectului
